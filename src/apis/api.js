import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyBGicXelkzkfAVxCB7O0q6CbOqy0r-AoYg",
    authDomain: "freshy-night.firebaseapp.com",
    databaseURL: "https://freshy-night.firebaseio.com",
    projectId: "freshy-night",
    storageBucket: "freshy-night.appspot.com",
    messagingSenderId: "1014824276895"
  }

  firebase.initializeApp(config)
  const database = firebase.database()

export default database