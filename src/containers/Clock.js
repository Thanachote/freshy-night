import React, { Component } from 'react'
import database from '../apis/api'
// import logo from '../assets/logo.png'

class Clock extends Component {
    constructor() {
        super()
        this.state = {
            isStart: false,
            timeId: '',
            buttonStyle: 'glyphicon glyphicon-play',
            minute: 0,
            second: 0,
            millisec: 0,
            timeData: null
        }
    }
    componentWillMount() {
        let saveData = []
        database.ref('/time').once('value')
        .then((res) => (
            res.forEach(function(element) {
                saveData.push(element.val())
            })
        ))
        this.setState({timeData: saveData})
    }
    startTimer() {
        let status = !this.state.isStart
        this.setState({isStart: status})
        if (status) {
            this.timer()
            this.setState({buttonStyle: 'glyphicon glyphicon-pause'})
        } else {
            this.stop()
            this.setState({buttonStyle: 'glyphicon glyphicon-play'})
        }
    }
    resetTimer() {
        this.setState({minute: 0})
        this.setState({second: 0})
        this.setState({millisec: 0})
    }
    timer() {
        let time = setInterval( () => {
            this.setState({millisec: this.state.millisec+1})
            this.calTimer()
        },10)
        this.setState({timeId: time})
    }
    stop() {
        clearInterval(this.state.timeId)
    }
    calTimer() {
        if (this.state.millisec === 100) {
            this.setState({millisec: 0})
            this.setState({second: this.state.second+1})
        }
        if (this.state.second === 60 ) {
            this.setState({second: 0})
            this.setState({minute: this.state.minute+1})
        }
    }
    saveTime(minute,second,millisec) {
        let time = minute+':'+second+':'+millisec
        database.ref('/time').push({
            time: time
        })
        this.setState({minute: 0, second:0, millisec: 0})
    }
    render() {
        let minute = 0
        if (this.state.minute <= 9) {
            minute = '0'+this.state.minute
        } else {
            minute = this.state.minute
        }
        let second = 0
        if (this.state.second <= 9) {
            second = '0'+this.state.second
        } else {
            second = this.state.second
        }
        let millisec = 0
        if (this.state.millisec <= 9) {
            millisec = '0'+this.state.millisec
        } else {
            millisec = this.state.millisec
        }
        let buttonStyle = this.state.buttonStyle
        let data = []
        if (this.state.timeData) {
            this.state.timeData.forEach((element,key) => (
                data[key] = element.time
            ))
        }
        return (
            <div className='container-fluid'>
                <div className='col-md-12'>
                    {/* <center><img src={logo} style={{width: '25%',height: '25%'}} alt='Logo'/></center> */}
                </div>
                <div className='col-md-12'>
                    <div className='col-md-6 col-md-offset-3'><h1 className='jumbotron'>{minute}:{second}:{millisec}</h1></div>
                </div>
                <div className='col-md-4 col-md-offset-4'>
                    <button className='btn btn-lg button' onClick={this.startTimer.bind(this)}><span className={buttonStyle}></span></button>&nbsp;&nbsp;&nbsp;
                    <button className='btn btn-lg button' onClick={this.resetTimer.bind(this)}><span className='glyphicon glyphicon-repeat'></span></button>&nbsp;&nbsp;&nbsp;
                    <button className='btn btn-lg button' onClick={this.saveTime.bind(this,minute,second,millisec)}><span className='glyphicon glyphicon-save'></span></button>
                </div> 
                {/* <div className='col-md-6 col-md-offset-3'>
                    <h1 className='watermark'>Developed By P'GUN IT'16</h1>
                </div> */}
            </div> 
        )
    }
}

export default Clock
