import React, { Component } from 'react'
import database from '../apis/api'
import '../styles/style2.css'

class SaveTime extends Component {
  constructor() {
    super()
    this.state = {
      data: [],
      test: 'test'
    }
  }
  componentDidMount() {
    let timeId = setInterval(() => this.fetch(), 1000)
    console.log(timeId)
  }
  fetch() {
    let self = this
    let saveData = []
    database
      .ref('/time')
      .once('value')
      .then(res => {
        res.forEach(function(element, i) {
          saveData.push(
            <li key={i} className="list-group-item">
              <p className="list">{element.val().time}</p>
            </li>
          )
        })
        self.setState({ data: saveData })
      })
  }
  reset() {
    let self = this
    database
      .ref('/time')
      .remove()
      .then(res => {
        self.setState({ data: [] })
      })
  }
  render() {
    return (
      <div className="container-fluid">
        <div className="col-md-12">
          <h1 className="title">Time data</h1>
          <button onClick={() => this.reset()}>Reset</button>
          <ul className="list-group">{this.state.data}</ul>
        </div>
      </div>
    )
  }
}

export default SaveTime
