import React from 'react'
import {Route, IndexRoute} from 'react-router'
import App from './components/App'
import Clock from './containers/Clock'
import SaveTime from './containers/SaveTime'

export default (
    <Route path='/' component={App}>
        <IndexRoute component={Clock} />
        <Route path='/view' component={SaveTime} />
    </Route>
)